import tkinter as tk
from tkinter import messagebox
import re

fonts = ('Courier New', 15, 'bold')
user = 'kiran'
passw = '123'

class Login:
    def __init__(self, root):
        self.root = root
        image_path = 'bg.png'
        self.img = tk.PhotoImage(file=image_path)
        self.image_label = tk.Label(root, image=self.img, bg='white')
        self.image_label.place(relx=0.5, rely=0.5, anchor='center')

        self.login_frame = tk.Frame(self.root, width=200, height=100, bg='hotpink')
        self.login_frame.pack(pady=100, side='right', anchor='e', padx=180)
        self.login_label = tk.Label(self.login_frame, text="LOGIN", font=('Helvetica', 40, 'bold'), fg='white', bg='hotpink')
        self.login_label.grid(row=0, column=1, columnspan=2)

        self.user_name = tk.Label(self.login_frame, text='NAME', font=('Helvetica', 20, 'bold'), bg='hotpink', fg='indigo', width=12)
        self.user_name.grid(row=3, column=1, pady=10)
        self.user_name_entry = tk.Entry(self.login_frame, width=15, font=fonts, bg='white')
        self.user_name_entry.grid(row=3, column=2, pady=10)

        self.user_pass = tk.Label(self.login_frame, text="PASSWORD", font=('Helvetica', 20, 'bold'), bg='hotpink', fg='indigo', width=12)
        self.user_pass.grid(row=5, column=1, pady=10)
        self.user_pass_entry = tk.Entry(self.login_frame, width=15, font=fonts, bg='white', show="*")
        self.user_pass_entry.grid(row=5, column=2, pady=10)

        self.admin_login_btn = tk.Button(self.login_frame, text='Orphanage', bg='white', fg='steel blue', font=fonts,
                                         command=self.open_orphanage_window, cursor='hand2', activebackground='blue')
        self.admin_login_btn.grid(row=7, column=0, columnspan=2)

        self.parent_login_btn = tk.Button(self.login_frame, text='Parent', bg='white', fg='steel blue', font=fonts,
                                          command=self.open_parent_window, cursor='hand2', activebackground='blue')
        self.parent_login_btn.grid(row=7, column=2, columnspan=2)

        '''self.admin_login_btn = tk.Button(self.login_frame, text='Orphanage', bg='white', fg='steel blue', font=fonts,
                                         command=self.check_login, cursor='hand2', activebackground='blue')
        self.admin_login_btn.grid(row=7, column=0, columnspan=2)
        self.parent_login_btn = tk.Button(self.login_frame, text='Parent', bg='white', fg='steel blue', font=fonts,
                                          command=self.check_login, cursor='hand2', activebackground='blue')
        self.parent_login_btn.grid(row=7, column=2, columnspan=2)'''

        self.additional_button = tk.Button(self.login_frame, text='Registration', bg='white', fg='blue', font=fonts,
                                           command=self.open_registration_window, cursor='hand2', activebackground='lightblue')
        self.additional_button.grid(row=9, column=0, columnspan=4, pady=20)

        self.reg_label = tk.Label(self.login_frame, text="Don't have an account? ", bg='hotpink', font=('Helvetica', 14, 'bold'), fg='white')
        self.reg_label.grid(row=8, column=0, columnspan=4, padx=5)

    def open_orphanage_window(self):
        # Implement code to open a new window for Orphanage button
        orphanage_window = tk.Toplevel(self.root)
        orphanage_window.title("Orphanage Window")
        # Add widgets and customize the window as needed.

    def open_parent_window(self):
        # Implement code to open a new window for Parent button
        parent_window = tk.Toplevel(self.root)
        parent_window.title("Parent Window")
        # Add widgets and customize the window as needed.


    def check_login(self):
        self.name = self.user_name_entry.get()
        self.password = self.user_pass_entry.get()

        if self.name == user:
            if self.password == passw:
                messagebox.showinfo("WELCOME", 'WELCOME USER')
                self.login_frame.destroy()
            else:
                messagebox.showerror("Wrong Password", 'Check your Password')
        else:
            messagebox.showerror('Wrong username', 'Invalid username')

    def open_registration_window(self):
        registration_window = tk.Toplevel(self.root)
        registration_window.title("Registration")
        registration_window.geometry("600x400")

        registration_frame = tk.Frame(registration_window, width=100, height=100)
        registration_frame.pack(expand=True, fill='both')

        reg_image_path = 'reg.png'
        reg_img = tk.PhotoImage(file=reg_image_path)
        reg_image_label = tk.Label(registration_frame, image=reg_img, bg='white', width=10, height=10)
        reg_image_label.image = reg_img
        reg_image_label.pack(expand=True, fill='both')

        reg_label = tk.Label(registration_frame, text="Registration Details", font=('Helvetica', 30, 'bold'), fg='hot pink', bg='white')
        reg_label.place(relx=0.4, rely=0.05, anchor='n')  # Set coordinates to overlap the image

        # Registration Details
        details_labels = ["First Name:", "Last Name:", "Mobile Number:", "Email:", "Username:", "Password:"]
        details_entries = []

        for i, label_text in enumerate(details_labels):
            label = tk.Label(registration_frame, text=label_text, font=('Helvetica', 14), fg='black' , bg='white')
            label.place(relx=0.25, rely=0.15 + i * 0.1, anchor='w')  # Adjust coordinates as needed
            entry = tk.Entry(registration_frame, font=('Helvetica', 12), bg='white')
            entry.place(relx=0.55, rely=0.15 + i * 0.1, anchor='e')  # Adjust coordinates as needed
            details_entries.append(entry)

        # Registration Button
        register_button = tk.Button(registration_frame, text='Finish Registration', bg='hotpink', fg='white', font=('Helvetica', 14),
                                    command=lambda: self.validate_and_register(registration_window, details_entries), cursor='hand2', activebackground='white')
        register_button.place(relx=0.4, rely=0.78, anchor='s')  # Set coordinates to overlap the image

    def validate_and_register(self, registration_window, details_entries):
        # Validation functions
        def is_valid_name(name):
            return name.isalpha()

        def is_valid_mobile_number(number):
            return number.isdigit() and len(number) == 10

        def is_valid_email(email):
            return re.match(r"[^@]+@[^@]+\.[^@]+", email)

        def is_valid_username(username):
            return username.isalnum()

        # Extract entries
        first_name, last_name, mobile_number, email, username, password = [entry.get() for entry in details_entries]

        # Validate input
        if not is_valid_name(first_name):
            self.show_error_message("Invalid First Name")
        elif not is_valid_name(last_name):
            self.show_error_message("Invalid Last Name")
        elif not is_valid_mobile_number(mobile_number):
            self.show_error_message("Invalid Mobile Number")
        elif not is_valid_email(email):
            self.show_error_message("Invalid Email")
        elif not is_valid_username(username):
            self.show_error_message("Invalid Username")
        else:
            # Perform registration logic here
            self.registration_successful(registration_window)

    def show_error_message(self, message):
        # Show error message dialog or handle the error as needed
        tk.messagebox.showerror("Error", message)

    def registration_successful(self, registration_window):
        messagebox.showinfo("Registration Successful", "Registration completed successfully!")
        registration_window.destroy()

if __name__ == "__main__":
    root = tk.Tk()
    root.title("Heart Bound")
    root.configure(bg="white")
    root.geometry('1920x1080')
    root.resizable(True, True)

    app = Login(root)

    root.mainloop()
